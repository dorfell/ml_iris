-------------------------------------------------------------------

     =========================================================
     ml_iris - Analysing the Iris data set with Machine Learning r
     =========================================================

                         List of modules
                         ---------------

Modules use in the analysis and development of the marchine learning models.

00_stat_cha_data
 - These are the programs used to make the statistical characterization of data.

01_knn   
 - These are the programs used to train the K-nearest neighbours algorithm with
   Iris data set.

docs
 - Documentation used for the development of codes.

bit  
 - Logbook Latex  sources. 
 - bit.pdf file including the ml_iris logbook.

README.md
 - You're reading it righ now  8-P. 
