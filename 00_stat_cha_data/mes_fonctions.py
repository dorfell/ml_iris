# -*- coding: utf-8 -*-
## 
# @file    
# @brief   Mes fonctions
# @details This module have the funtions needed to run the knn code.
# @date    2020/05/18
# @version 0.1
"""@package docstring
"""
################################
## Mes fonctions
## by Dorfell Parra 
## 2020/05/18
#################################
import os
import numpy as np
import matplotlib.pyplot as plt

# sklearn
from sklearn.neighbors import KNeighborsClassifier as KNN

import matplotlib as mpl
mpl.rcParams['legend.fontsize']     = 14;
mpl.rcParams['axes.labelsize']      = 14;
mpl.rcParams['xtick.labelsize']     = 12;
mpl.rcParams['ytick.labelsize']     = 12;
mpl.rcParams['text.usetex']         = True;
mpl.rcParams['font.family']         = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{mathrsfs}'];
mpl.rcParams.update({'font.size': 10});


# Plot parameters pairs
def plt_pair(X_train, y_train, X_test, y_test):
    ''' Function for plotting the input data: parameters pairs.  
        X_train: data for training.
        y_train: labels for training.
        X_test: data for testing.
        y_test: labels for testing. '''

    # Min, max and means 
    #----------------------------------------
    print("                                      ");
    Y_min, Y_max   = X_train[:, 0].min() - .5, X_train[:, 0].max() + .5; # Y 
    mu_min, mu_max = X_train[:, 1].min() - .5, X_train[:, 1].max() + .5; # mu
    si_min, si_max = X_train[:, 2].min() - .5, X_train[:, 2].max() + .5; # sigma
    b0_min, b0_max = X_train[:, 3].min() - .5, X_train[:, 3].max() + .5; # b0
    b1_min, b1_max = X_train[:, 4].min() - .5, X_train[:, 4].max() + .5; # b1
    print("Y_min,  Y_max  --> ", Y_min,   Y_max);
    print("mu_min, mu_min --> ", mu_min, mu_max); 
    print("si_min, si_max --> ", si_min, si_max);
    print("b0_min, b0_max --> ", b0_min, b0_max);
    print("b1_min, b1_max --> ", b1_min, b1_max);

     # Creating figure
    figure = plt.figure(figsize=(15, 10));
    labels=[r"$PVC~+~agua$", r"$PVC$"]; # labels for 2 classes

    # mu vs. Y
    xx,yy = np.meshgrid( np.arange(Y_min,Y_max,0.1), np.arange(mu_min,mu_max,0.1) );
    ax = plt.subplot(2, 5, 1);
    #ax.set_title(r"$\mu~vs.~Y$");
    #ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap="jet", edgecolors='k');
    leg = ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  0], X_test[:,  1], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$Y~x10^3~(c)$"); ax.set_ylabel(r"$\mu~(cm)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks
    ax.legend(handles=leg.legend_elements()[0], labels=labels, loc="best", fontsize=6);

    # sigma vs. Y
    xx,yy = np.meshgrid( np.arange(Y_min,Y_max,0.1), np.arange(si_min,si_max,0.1) );
    ax = plt.subplot(2, 5, 2);
    #ax.set_title(r"$\sigma~vs.~Y$");
    ax.scatter(X_train[:, 0], X_train[:, 2], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  0], X_test[:,  2], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$Y~x10^3~(c)$"); ax.set_ylabel(r"$\sigma~(cm)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # b0 vs. Y
    xx,yy = np.meshgrid( np.arange(Y_min,Y_max,0.1), np.arange(b0_min,b0_max,0.1) );
    ax = plt.subplot(2, 5, 3);
    #ax.set_title(r"$b_0~vs.~Y$");
    ax.scatter(X_train[:, 0], X_train[:, 3], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  0], X_test[:,  3], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$Y~x10^3~(c)$"); ax.set_ylabel(r"$b_0~x10^3~(c)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # b1 vs. Y
    xx,yy = np.meshgrid( np.arange(Y_min,Y_max,0.1), np.arange(b1_min,b1_max,0.1) );
    ax = plt.subplot(2, 5, 4);
    #ax.set_title(r"$b_1~vs.~Y$");
    ax.scatter(X_train[:, 0], X_train[:, 4], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  0], X_test[:,  4], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$Y~x10^3~(c)$"); ax.set_ylabel(r"$b_1~(c/cm)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # sigma vs. mu
    xx,yy = np.meshgrid( np.arange(mu_min,mu_max,0.1), np.arange(si_min,si_max,0.1) );
    ax = plt.subplot(2, 5, 5);
    #ax.set_title(r"$\sigma~vs.~\mu$");
    ax.scatter(X_train[:, 1], X_train[:, 2], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  1], X_test[:,  2], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\mu~(cm)$"); ax.set_ylabel(r"$\sigma~(cm)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # b0 vs. mu
    xx,yy = np.meshgrid( np.arange(mu_min,mu_max,0.1), np.arange(b0_min,b0_max,0.1) );
    ax = plt.subplot(2, 5, 6);
    #ax.set_title(r"$b_0~vs.~\mu$");
    ax.scatter(X_train[:, 1], X_train[:, 3], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  1], X_test[:,  3], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\mu~(cm)$"); ax.set_ylabel(r"$b_0~x10^3~(c)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # b1 vs. mu
    xx,yy = np.meshgrid( np.arange(mu_min,mu_max,0.1), np.arange(b1_min,b1_max,0.1) );
    ax = plt.subplot(2, 5, 7);
    #ax.set_title(r"$b_1~vs.~mu$");
    ax.scatter(X_train[:, 1], X_train[:, 4], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  1], X_test[:,  4], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\mu~(cm)$"); ax.set_ylabel(r"$b_1~(c/cm)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # b0 vs. sigma
    xx,yy = np.meshgrid( np.arange(si_min,si_max,0.1), np.arange(b0_min,b0_max,0.1) );
    ax = plt.subplot(2, 5, 8);
    #ax.set_title(r"$b_0~vs.~sigma$");
    ax.scatter(X_train[:, 2], X_train[:, 3], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  2], X_test[:,  3], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\sigma~(cm)$"); ax.set_ylabel(r"$b_0~x10^3~(c)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # b1 vs. sigma
    xx,yy = np.meshgrid( np.arange(si_min,si_max,0.1), np.arange(b1_min,b1_max,0.1) );
    ax = plt.subplot(2, 5, 9);
    #ax.set_title(r"$b_1~vs.~sigma$");
    ax.scatter(X_train[:, 2], X_train[:, 4], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  4], X_test[:,  4], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\sigma~(cm)$"); ax.set_ylabel(r"$b_1~(c/cm)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # b1 vs. b0
    xx,yy = np.meshgrid( np.arange(b0_min,b0_max,0.1), np.arange(b1_min,b1_max,0.1) );
    ax = plt.subplot(2, 5, 10);
    #ax.set_title(r"$b_1~vs.~b_0$");
    ax.scatter(X_train[:, 3], X_train[:, 4], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  3], X_test[:,  4], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$b_0~x10^3~(c)$"); ax.set_ylabel(r"$b_1~(c/cm)$");
    ax.set_xlim(xx.min(), xx.max()); ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks

    # Saving the image
    plt.tight_layout();
    plt.savefig("img/ds1.png", bbox_inches="tight");
    plt.show();
    os.system("convert img/ds1.png img/ds1.eps");



# Plot the KNN decision boundaries
def knn_dec(X_train, y_train, X_test, y_test, k, data):
    ''' Function for plotting the KNN decision boundaries.  
        X_train: data for training.
        y_train: labels for training.
        X_test: data for testing.
        y_test: labels for testing.
        k: number of neighbors.
        data: Source of data; seeds, synthetic. '''

    # Min, max and means 
    #----------------------------------------
    print("                                      ");
    Y_min,  Y_max  = X_train[:, 0].min() - .5, X_train[:, 0].max() + .5; # Y 
    mu_min, mu_max = X_train[:, 1].min() - .5, X_train[:, 1].max() + .5; # mu
    si_min, si_max = X_train[:, 2].min() - .5, X_train[:, 2].max() + .5; # sigma
    b0_min, b0_max = X_train[:, 3].min() - .5, X_train[:, 3].max() + .5; # b0
    b1_min, b1_max = X_train[:, 4].min() - .5, X_train[:, 4].max() + .5; # b1
    Y_mean  = np.mean(X_train[:, 0]); mu_mean = np.mean(X_train[:, 1]); 
    si_mean = np.mean(X_train[:, 2]); b0_mean = np.mean(X_train[:, 3]); 
    b1_mean = np.mean(X_train[:, 4]); 
    print("Y_min,  Y_max,  Y_mean  --> ", Y_min,   Y_max, Y_mean );
    print("mu_min, mu_min, mu_mean --> ", mu_min, mu_max, mu_mean); 
    print("si_min, si_max, si_mean --> ", si_min, si_max, si_mean);
    print("b0_min, b0_max, b0_mean --> ", b0_min, b0_max, b0_mean);
    print("b1_min, b1_max, b1_mean --> ", b1_min, b1_max, b1_mean);


    # Initializing the KNN 
    knn = KNN(n_neighbors=k);
 
    # Fitting with train data
    k = str(k);
    print("Training knn for k"+k+"...");
    knn = knn.fit(X_train, y_train);

    # Validating with test data
    knn_score = knn.score(X_test, y_test);
    print("knn k"+k+" score: ", knn_score*100,"%");

    # Creating figure
    figure = plt.figure(figsize=(15, 9));
    labels=[r"$PVC~+~agua$", r"$PVC$"]; # labels for 2 classes

    # mu vs. Y
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(Y_min,Y_max,0.1), np.arange(mu_min,mu_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    #YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    #mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    sisi = np.ones(xx.shape, dtype=float) * si_mean;
    b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ xx.ravel(), yy.ravel(), sisi.ravel(), b0b0.ravel(), b1b1.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 1);
    #ax.set_title(r"$\mu~vs.~Y$");
    cf = ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    #ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap="jet", edgecolors='k');
    leg = ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  0], X_test[:,  1], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$Y~x10^3~(c)$"); ax.set_ylabel(r"$\mu~(cm)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());
    #ax.set_xticks(()); ax.set_yticks(()); # without ticks
    ax.legend(handles=leg.legend_elements()[0], labels=labels, loc="best", fontsize=6);
    plt.colorbar(cf, ax=ax);

    # sigma vs. Y
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(Y_min,Y_max,0.1), np.arange(si_min,si_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    #YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    #sisi = np.ones(xx.shape, dtype=float) * si_mean;
    b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ xx.ravel(), mumu.ravel(), yy.ravel(), b0b0.ravel(), b1b1.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 2);
    #ax.set_title(r"$\sigma~vs.~Y$");
    ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 0], X_train[:, 2], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  0], X_test[:,  2], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$Y~x10^3~(c)$"); ax.set_ylabel(r"$\sigma~(cm)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # b0 vs. Y
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(Y_min,Y_max,0.1), np.arange(b0_min,b0_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    #YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    sisi = np.ones(xx.shape, dtype=float) * si_mean;
    #b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ xx.ravel(), mumu.ravel(), sisi.ravel(), yy.ravel(), b1b1.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 3);
    #ax.set_title(r"$b_0~vs.~Y$");
    ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 0], X_train[:, 3], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  0], X_test[:,  3], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$Y~x10^3~(c)$"); ax.set_ylabel(r"$b_0~x10^3~(c)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # b1 vs. Y
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(Y_min,Y_max,0.1), np.arange(b1_min,b1_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    #YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    sisi = np.ones(xx.shape, dtype=float) * si_mean;
    b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    #b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ xx.ravel(), mumu.ravel(), sisi.ravel(), b0b0.ravel(), yy.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 4);
    #ax.set_title(r"$b_1~vs.~Y$");
    ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 0], X_train[:, 4], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  0], X_test[:,  4], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$Y~x10^3~(c)$"); ax.set_ylabel(r"$b_1~(c/cm)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # sigma vs. mu
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(mu_min,mu_max,0.1), np.arange(si_min,si_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    #mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    #sisi = np.ones(xx.shape, dtype=float) * si_mean;
    b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ YY.ravel(), xx.ravel(), yy.ravel(), b0b0.ravel(), b1b1.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 5);
    #ax.set_title(r"$\sigma~vs.~\mu$");
    cf = ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 1], X_train[:, 2], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  1], X_test[:,  2], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\mu~(cm)$"); ax.set_ylabel(r"$\sigma~(cm)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # b0 vs. mu 
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(mu_min,mu_max,0.1), np.arange(b0_min,b0_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    #mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    sisi = np.ones(xx.shape, dtype=float) * si_mean;
    #b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ YY.ravel(), xx.ravel(), sisi.ravel(), yy.ravel(), b1b1.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 6);
    #ax.set_title(r"$b_0~vs.~mu$");
    ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 1], X_train[:, 3], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  1], X_test[:,  3], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\mu~(cm)$"); ax.set_ylabel(r"$b_0~x10^3~(c)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # b1 vs. mu
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(mu_min,mu_max,0.1), np.arange(b1_min,b1_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    #mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    sisi = np.ones(xx.shape, dtype=float) * si_mean;
    b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    #b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ YY.ravel(), xx.ravel(), sisi.ravel(), b0b0.ravel(), yy.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 7);
    #ax.set_title(r"$b_1~vs.~\mu$");
    ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 1], X_train[:, 4], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  1], X_test[:,  4], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\mu~(cm)$"); ax.set_ylabel(r"$b_1~(c/cm)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # b0 vs. sigma 
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(si_min,si_max,0.1), np.arange(b0_min,b0_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    #sisi = np.ones(xx.shape, dtype=float) * si_mean;
    #b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ YY.ravel(), mumu.ravel(), xx.ravel(), yy.ravel(), b1b1.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 8);
    #ax.set_title(r"$b_0~vs.~\sigma$");
    ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 2], X_train[:, 3], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  2], X_test[:,  3], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\sigma~(cm)$"); ax.set_ylabel(r"$b_0~x10^3~(c)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # b1 vs. sigma
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(si_min,si_max,0.1), np.arange(b1_min,b1_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    #sisi = np.ones(xx.shape, dtype=float) * si_mean;
    b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    #b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ YY.ravel(), mumu.ravel(), xx.ravel(), b0b0.ravel(), yy.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 9);
    #ax.set_title(r"$b_1~vs.~\sigma$");
    ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 2], X_train[:, 4], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  2], X_test[:,  4], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$\sigma~(cm)$"); ax.set_ylabel(r"$b_1~(c/cm)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # b1 vs. b0
    #-----------------------------
    xx,yy = np.meshgrid( np.arange(b0_min,b0_max,0.1), np.arange(b1_min,b1_max,0.1) );
    #print(xx); print(len(xx)); print(xx.shape);
    YY   = np.ones(xx.shape, dtype=float) * Y_mean;
    mumu = np.ones(xx.shape, dtype=float) * mu_mean;
    sisi = np.ones(xx.shape, dtype=float) * si_mean;
    #b0b0 = np.ones(xx.shape, dtype=float) * b0_mean;
    #b1b1 = np.ones(xx.shape, dtype=float) * b1_mean;

    # -- Ploting the decision boundary
    Z = knn.predict_proba( np.c_[ YY.ravel(), mumu.ravel(), sisi.ravel(), xx.ravel(), yy.ravel() ] )[:, 1];
    Z = Z.reshape(xx.shape);
    ax = plt.subplot(2, 5, 10);
    #ax.set_title(r"$b_1~vs.~b_0$");
    ax.contourf(xx, yy, Z, cmap="jet", alpha=0.8); # Boundaries
    ax.scatter(X_train[:, 3], X_train[:, 4], c=y_train, cmap="jet", edgecolors='k');
    #ax.scatter(X_test[:,  3], X_test[:,  4], c=y_test,  cmap="jet", edgecolors='k', alpha=0.6);
    ax.set_xlabel(r"$b_0~x10^3~(c)$"); ax.set_ylabel(r"$b_1~(c/cm)$");
    ax.set_xlim(xx.min(), xx.max());  ax.set_ylim(yy.min(), yy.max());

    # Saving the image
    #cax = plt.axes([0.95, 0.0, 0.01, 0.4]);
    #plt.colorbar(cf, cax=cax);
    plt.tight_layout();
    plt.savefig("img/knn_k"+k+"_ds1_"+data+".png", bbox_inches="tight");
    plt.show();
    os.system("convert img/knn_k"+k+"_ds1_"+data+".png img/knn_k"+k+"_ds1_"+data+".eps");
