# -*- coding: utf-8 -*-
## 
# @file    00_scd.py   
# @brief   Statistical Characterization of data 
# @details Characterization of data ... 
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/06/11
# @version 0.1
"""@package docstring
"""
################################
## 00_scd
## by Dorfell Parra 
## 2020/06/11
#################################

import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12
mpl.rcParams['axes.labelsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['mathtext.fontset'] = 'dejavusans'
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{mathrsfs}']
mpl.rcParams.update({'font.size': 12})

import numpy as np
import pandas as pd
import seaborn as sns
sns.set(style="ticks", color_codes=True);
from sklearn import datasets

print("**************************************");
print("* Data chacterization    ...         *");
print("**************************************");

# Loading dataset 1
#---------------------------------------
print("                                      ");
print("* Loading data           ...         *");
print("**************************************");

print("* sepal length (cm), sepal width (cm), petal length (cm), petal width (cm), species *");

iris = datasets.load_iris();
#print(iris.data); print(iris.target); print(iris.target_names);

iris_label = []; # Species labels
for idx in range(0, len(iris.target)):
  iris_label.append( iris.target_names[ iris.target[idx] ] );

# Creating Dataframe
col_nam = [r"$sepal~length~(cm)$", r"$sepal~width~(cm)$", r"$petal~length~(cm)$", r"$petal~width~(cm)$"];
iris_df = pd.DataFrame(data=iris.data, columns=col_nam);
iris_df["species"] = iris_label; # Adding labels to dataframe
print(iris_df);


# Ploting the scatter matrix
#---------------------------------------
#figure = plt.figure(figsize=(15, 9));
sns.pairplot(iris_df, hue="species");
plt.savefig("img/scd_scatter.png", bbox_inches="tight");
plt.show();

# Ploting the parameters distribution 
#---------------------------------------
figure = plt.figure(figsize=(15, 9));
# sepal length
plt.subplot(2, 2, 1);
sns.distplot(iris_df[r"$sepal~length~(cm)$"]);

# sepal width
plt.subplot(2, 2, 2);
sns.distplot(iris_df[r"$sepal~width~(cm)$"]);

# petal length
plt.subplot(2, 2, 3);
sns.distplot(iris_df[r"$petal~length~(cm)$"]);

# petal width
plt.subplot(2, 2, 4);
sns.distplot(iris_df[r"$petal~width~(cm)$"]);

plt.savefig("img/scd_distplot.png", bbox_inches="tight");
plt.show();


# Ploting the parameters distribution  per species
#---------------------------------------
#figure = plt.figure(figsize=(15, 9));
# sepal length
f = sns.FacetGrid(iris_df, col="species");
f.map(sns.distplot, r"$sepal~length~(cm)$");
plt.savefig("img/scd_distplot_sl.png", bbox_inches="tight");

# sepal width
f = sns.FacetGrid(iris_df, col="species");
f.map(sns.distplot, r"$sepal~width~(cm)$");
plt.savefig("img/scd_distplot_sw.png", bbox_inches="tight");

# petal length
f = sns.FacetGrid(iris_df, col="species");
f.map(sns.distplot, r"$petal~length~(cm)$");
plt.savefig("img/scd_distplot_pl.png", bbox_inches="tight");

# petal width
f = sns.FacetGrid(iris_df, col="species");
f.map(sns.distplot, r"$petal~width~(cm)$");
plt.savefig("img/scd_distplot_pw.png", bbox_inches="tight");

plt.show();


# Ploting the parameters pairs with distributions
#---------------------------------------
#figure = plt.figure(figsize=(15, 9));
# sepal length
#f = sns.jointplot(data=iris_df, x= r"$sepal~length~(cm)$", y= r"$sepal~width~(cm)$",kind="reg", color="b");
#plt.savefig("img/scd_jointplot_sl_sw.png", bbox_inches="tight");
#plt.show();


# Ploting the parameters correlation
#---------------------------------------
col_nam_abv = {r"$sepal~length~(cm)$":r"$sepal~len$", r"$sepal~width~(cm)$": r"$sepal~wid$", r"$petal~length~(cm)$":r"$petal~len$", r"$petal~width~(cm)$": r"$petal~wid$"};
iris_df = iris_df.rename(columns=col_nam_abv);
print(iris_df);
print(iris_df.corr());

# use matplotlib 3.2.1
#figure = plt.figure(figsize=(15, 9));
sns.heatmap(iris_df.corr(), annot=True, square=True, fmt="0.2f");
plt.savefig("img/scd_corr.png", bbox_inches="tight");
plt.show();


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
