-------------------------------------------------------------------

     =========================================================
     ml_iris/00_stat_cha_data - Statistical Characterization of Data
     =========================================================

                         List of files
                         ---------------

These are the programs used to make the statistical characterization of data.


00_scd.py 
 - Program to create the scatter matrix  plots, distributions plots and correlations
   plots of parameters for  the Iris  data set.

mes_fonctions.py 
 - Module with the functions needed to run the 00_scd.py program.

img 
 - Images created with these programs.

README.md
 - You're reading it righ now  8-P. 
