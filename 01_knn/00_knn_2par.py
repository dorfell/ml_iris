# -*- coding: utf-8 -*-
## 
# @file    00_knn.py 
# @brief   KNN : Iris data set
# @details Module for testing the Knn algorithm performance with the Iris data set
#          using 2 parameters.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/06/11
# @version 0.1
"""@package docstring
"""
################################
## 00_knn_2par.py
## by Dorfell Parra
## 2020/06/11
#################################

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# sklearn
from sklearn import datasets

from  mes_fonctions import *


print("**************************************");
print("* K-Nearest Neighbors    ...         *");
print("**************************************");

# Loading dataset 1
#---------------------------------------
print("                                      ");
print("* Loading data           ...         *");
print("**************************************");

iris = datasets.load_iris();
#print(iris.data); print(iris.target); print(iris.target_names);

iris_species = []; # Iris species labels
for idx in range(0, len(iris.target)):
  iris_species.append( iris.target_names[ iris.target[idx] ] );

# Creating Dataframe
iris_col = [r"$sepal~length~(cm)$", r"$sepal~width~(cm)$", r"$petal~length~(cm)$", r"$petal~width~(cm)$"];
iris_df = pd.DataFrame(data=iris.data, columns=iris_col);
iris_df["labels"]  = iris.target;  # Adding labels columns to dataframe
iris_df["species"] = iris_species; # Adding species column to dataframe
print(iris_df);


# K-nearest neighbors algorithme  
#=======================================
# Separate data from labels
lab_df = iris_df["labels"];
iris_df.drop("species", axis=1, inplace=True);
iris_df.drop("labels", axis=1, inplace=True);


# Training with 10%, data set.
#---------------------------------------
# Data frame for scores
scr_df = np.zeros( [10, 6] ); # 10 k-values, 6 planes (e.g. sw vs sl, pl vs sl, etc.)
# Data set percentage for training = (1-test_prc)*100.
prc = 0.9; # E.g.: test: 0.9 (90%) --> train: 0.1 (10%)
# knn for k=1,2, ..., 10
for k in range(1,11):
#for k in range(1, 3):
  lt_score = knn_dec_2par(iris_df, lab_df, prc, k);  	
  scr_df[k-1, :] = lt_score;  
#print(scr_df);
scr_plot(scr_df, prc); # Plot scores


# Training with 40%, data set.
#---------------------------------------
# Data frame for scores
scr_df = np.zeros( [10, 6] ); # 10 k-values, 6 planes (e.g. sw vs sl, pl vs sl, etc.)
# Data set percentage for training = (1-test_prc)*100.
prc = 0.6; # E.g.: test: 0.6 (60%) --> train: 0.4 (40%)
# knn for k=1,2, ..., 10
for k in range(1,11):
#for k in range(1, 3):
  lt_score = knn_dec_2par(iris_df, lab_df, prc, k);  	
  scr_df[k-1, :] = lt_score;  
#print(scr_df);
scr_plot(scr_df, prc); # Plot scores


# Training with 70%, data set.
#---------------------------------------
# Data frame for scores
scr_df = np.zeros( [10, 6] ); # 10 k-values, 6 planes (e.g. sw vs sl, pl vs sl, etc.)
# Data set percentage for training = (1-test_prc)*100.
prc = 0.3; # E.g.: test: 0.3 (30%) --> train: 0.7 (70%)
# knn for k=1,2, ..., 10
for k in range(1,11):
#for k in range(1, 3):
  lt_score = knn_dec_2par(iris_df, lab_df, prc, k);  	
  scr_df[k-1, :] = lt_score;  
#print(scr_df);
scr_plot(scr_df, prc); # Plot scores


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
