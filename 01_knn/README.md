-------------------------------------------------------------------

     =========================================================
     ml_iris/01_knn - K-nearest neighbours with Iris data set
     =========================================================

                         List of files
                         ---------------

These are the programs used to train the K-nearest neighbours algorithm with the Iris 
data set.


00_knn_2par.py 
 - Program to train the knn algorithm for k=1,2,...,10, and training data equals 
   to the 10%. 40% and 70% of Iris data set, using only 2 parameters at time. 

01_knn.py 
 - Program to train the knn algorithm for k=1,2,...,10, and training data equals 
   to the 10%. 40% and 70% of Iris data set, using only 24 parameters at time. 

mes_fonctions.py 
 - Module with the functions needed to run the 00_knn_2par.py and 01_knn.py programs.

img_2par 
 - Images created with the programs 00_knn_2par.py

img 
 - Images created with the programs 01_knn.py

00_knn_2par.log
 - Execution log for the 00_knn_2par.log program.

README.md
 - You're reading it righ now  8-P. 
