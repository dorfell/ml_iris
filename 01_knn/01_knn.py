# -*- coding: utf-8 -*-
## 
# @file    00_knn.py 
# @brief   KNN : Iris data set
# @details Module for testing the Knn algorithm performance with the iris 
#          data set 4 parameters.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/06/14
# @version 0.1
"""@package docstring
"""
################################
## 00_knn
## by Dorfell Parra
## 2020/06/14
#################################

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# sklearn
from sklearn import datasets

from  mes_fonctions import *


print("**************************************");
print("* K-Nearest Neighbors    ...         *");
print("**************************************");

# Loading dataset 1
#---------------------------------------
print("                                      ");
print("* Loading data           ...         *");
print("**************************************");

iris = datasets.load_iris();
#print(iris.data); print(iris.target); print(iris.target_names);

iris_species = []; # Iris species labels
for idx in range(0, len(iris.target)):
  iris_species.append( iris.target_names[ iris.target[idx] ] );

# Creating Dataframe
iris_col = [r"$sepal~length~(cm)$", r"$sepal~width~(cm)$", r"$petal~length~(cm)$", r"$petal~width~(cm)$"];
iris_df = pd.DataFrame(data=iris.data, columns=iris_col);
iris_df["labels"]  = iris.target;  # Adding labels columns to dataframe
iris_df["species"] = iris_species; # Adding species column to dataframe
print(iris_df);


# K-nearest neighbors algorithme  
#=======================================
# Separate data from labels
lab_df = iris_df["labels"];
iris_df.drop("species", axis=1, inplace=True);
iris_df.drop("labels", axis=1, inplace=True);

# Data frame for scores
scr_df = np.zeros( [10, 3] ); # 10 k-values, 3 training data (i.e. 10%, 40%, 70%.)


# Training with 10%, data set.
#---------------------------------------
# Data set percentage for training = (1-test_prc)*100.
prc = 0.9; # E.g.: test: 0.9 (90%) --> train: 0.1 (10%)
# knn for k=1,2, ..., 10
for k in range(1,11):
#for k in range(1, 3):
  score = knn_dec(iris_df, lab_df, prc, k);  	
  scr_df[k-1, 0] = score;  


# Training with 40%, data set.
#---------------------------------------
# Data set percentage for training = (1-test_prc)*100.
prc = 0.6; # E.g.: test: 0.6 (60%) --> train: 0.4 (40%)
# knn for k=1,2, ..., 10
for k in range(1,11):
#for k in range(1, 3):
  score = knn_dec(iris_df, lab_df, prc, k);  	
  scr_df[k-1, 1] = score;  


# Training with 70%, data set.
#---------------------------------------
# Data set percentage for training = (1-test_prc)*100.
prc = 0.3; # E.g.: test: 0.3 (30%) --> train: 0.7 (70%)
# knn for k=1,2, ..., 10
for k in range(1,11):
#for k in range(1, 3):
  score = knn_dec(iris_df, lab_df, prc, k);  	
  scr_df[k-1, 2] = score;  


# Ploting the scores values
#----------------------------------------
scr_lt = [r"$train\_data~10\%$", r"$train\_data~40\%$", r"$train\_data~70\%$"];
scr_df = pd.DataFrame(data=scr_df, columns=scr_lt);
print("Scores data frame: ", scr_df);
k_lt = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; 

figure = plt.figure(figsize=(15, 9));

ax = plt.subplot(1, 3, 1); 
plt.scatter(k_lt, scr_df[r"$train\_data~10\%$"], cmap="jet", edgecolors='k');
plt.plot(k_lt, scr_df[r"$train\_data~10\%$"]);
ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~train~data~10\%$");
ax.set_ylim( scr_df[r"$train\_data~10\%$"].min()-0.2, scr_df[r"$train\_data~10\%$"].max()+0.1 );

ax = plt.subplot(1, 3, 2); 
plt.scatter(k_lt, scr_df[r"$train\_data~40\%$"], cmap="jet", edgecolors='k');
plt.plot(k_lt, scr_df[r"$train\_data~40\%$"]);
ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~train~data~40\%$");
ax.set_ylim( scr_df[r"$train\_data~40\%$"].min()-0.2, scr_df[r"$train\_data~40\%$"].max()+0.1 );

ax = plt.subplot(1, 3, 3); 
plt.scatter(k_lt, scr_df[r"$train\_data~70\%$"], cmap="jet", edgecolors='k');
plt.plot(k_lt, scr_df[r"$train\_data~70\%$"]);
ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~train~data~70\%$");
ax.set_ylim( scr_df[r"$train\_data~70\%$"].min()-0.2, scr_df[r"$train\_data~70\%$"].max()+0.1 );
# Saving the image
plt.tight_layout();

# k: number of  neighbors, td: train data percentage.
plt.savefig("img/knn_scores.png", bbox_inches="tight");
plt.show();


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
