# -*- coding: utf-8 -*-
## 
# @file    
# @brief   Mes fonctions
# @details This module have the funtions needed to run the knn code.
# @date    2020/06/12
# @version 0.1
"""@package docstring
"""
################################
## Mes fonctions
## by Dorfell Parra 
## 2020/06/12
#################################
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# sklearn
from sklearn.neighbors import KNeighborsClassifier

from sklearn.model_selection import train_test_split


import matplotlib as mpl
mpl.rcParams['legend.fontsize']     = 14;
mpl.rcParams['axes.labelsize']      = 14;
mpl.rcParams['xtick.labelsize']     = 12;
mpl.rcParams['ytick.labelsize']     = 12;
mpl.rcParams['text.usetex']         = True;
mpl.rcParams['font.family']         = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{mathrsfs}'];
mpl.rcParams.update({'font.size': 10});


# Function to select pertinent samples.
def fun_is(train_df, prediction):
  ''' This function creates a boolean column to select the
      samples that will be plotted in each plane. 
      train_df: Dataframe with training data. 
      prediction: Predictions for the meshgrid..  
      is_in: column returned.                          '''

  # Checking each prediction value
  if (0 in prediction):
    is_setosa = (train_df["labels"]== 0);
  else: 
    is_setosa = train_df["labels"].astype(bool) & False;

  if (1 in prediction):
    is_versicolor = (train_df["labels"]== 1);
  else: 
    is_versicolor = train_df["labels"].astype(bool)& False;

  if (2 in prediction):
    is_virginica = (train_df["labels"]== 2);
  else: 
    is_virginica = train_df["labels"].astype(bool) & False;
   
  is_in = is_setosa | is_versicolor |  is_virginica;
  return is_in;

# KNN training and validation.
def knn_clf(X_train, y_train, X_test, y_test, k):
  ''' This function  trains and test the knn and returns 
      the knn trainned with its score.
      X_train: Training data, y_train: training labels. 
      X_test: Testing data,   y_test:  testing labels.  
      k: number of neighbors.                          '''
  knn = KNeighborsClassifier(n_neighbors=k);  # Initializing the KNN 
  knn = knn.fit(X_train, y_train);            # Fitting with train data
  knn_score = knn.score(X_test, y_test);      # Validating with test data
  print("Validation score: %0.2f" % (knn_score*100), "%" );
  return knn, knn_score;


# Min, max and mean from data frame.
def df_min_max_mean(data_df):
  ''' This function  returns the min, max and mean of each 
      parameter of the data frame. 
      data_df: data set in Pandas DataFrame format.        '''

  # Min, max and means 
  #----------------------------------------
  sl_min  = data_df[r"$sepal~length~(cm)$"].min();  # Sepal length min
  sl_max  = data_df[r"$sepal~length~(cm)$"].max();  # Sepal length max
  sl_mean = data_df[r"$sepal~length~(cm)$"].mean(); # Sepal length mean
  print("sepal length --> min: %0.2f, max: %0.2f, mean: %0.2f" % (sl_min, sl_max, sl_mean));

  sw_min  = data_df[r"$sepal~width~(cm)$"].min();   # Sepal width min
  sw_max  = data_df[r"$sepal~width~(cm)$"].max();   # Sepal width max
  sw_mean = data_df[r"$sepal~width~(cm)$"].mean();  # Sepal width mean
  print("sepal width  --> min: %0.2f, max: %0.2f, mean: %0.2f" % (sw_min, sw_max, sw_mean));

  pl_min  = data_df[r"$petal~length~(cm)$"].min();  # Petal length min
  pl_max  = data_df[r"$petal~length~(cm)$"].max();  # Petal length max
  pl_mean = data_df[r"$petal~length~(cm)$"].mean(); # Petal length mean
  print("petal length --> min: %0.2f, max: %0.2f, mean: %0.2f" % (pl_min, pl_max, pl_mean));

  pw_min  = data_df[r"$petal~width~(cm)$"].min();   # Petal width min
  pw_max  = data_df[r"$petal~width~(cm)$"].max();   # Petal wiidth max
  pw_mean = data_df[r"$petal~width~(cm)$"].mean();  # Petal width mean
  print("petal width  --> min: %0.2f, max: %0.2f, mean: %0.2f" % (pw_min, pw_max, pw_mean));

  return (sl_min, sl_max, sl_mean, sw_min, sw_max, sw_mean, pl_min, pl_max, pl_mean, pw_min, pw_max, pw_mean);


# Plot the KNN decision boundaries training 2 parameters
def knn_dec_2par(data_df, lab_df, prc, k):
  ''' Function for plotting the KNN decision boundaries.  
      data_df: data set in Pandas DataFrame format.
      lab_df: Data label in Pandas DataFrame format.
      prc: Percentage data for testing.
      k: number of neighbors.                        '''
    
  labels=[r"$setosa$", r"$versicolor$", r"$virginica$"]; # labels for 3 classes

  # Min, max and mean from data frame.
  #----------------------------------------
  (sl_min, sl_max, sl_mean, sw_min, sw_max, sw_mean, pl_min, pl_max, pl_mean, pw_min, pw_max, pw_mean) =  df_min_max_mean(data_df);
  print("                            ");
  # sl, sw, pl, pw  arange for meshgrid
  #----------------------------------------
  sl = np.arange( sl_min-0.1, sl_max+0.1, 0.01);
  sw = np.arange( sw_min-0.1, sw_max+0.1, 0.01);
  pl = np.arange( pl_min-0.1, pl_max+0.1, 0.01);
  pw = np.arange( pw_min-0.1, pw_max+0.1, 0.01);

  #  sw  vs. sl
  #========================================
  # Training and testing the knn
  #----------------------------------------
  X_train, X_test, y_train, y_test = train_test_split( data_df.loc[:, (r"$sepal~length~(cm)$", r"$sepal~width~(cm)$") ], lab_df, test_size=prc, shuffle=True, random_state=42 );
  print("Training knn: k"+str(k)+", train data "+str((1-prc)*100)+"%, parameters: ");
  print( data_df.loc[:, (r"$sepal~length~(cm)$", r"$sepal~width~(cm)$") ] );
  knn, scr_slsw = knn_clf(X_train, y_train, X_test, y_test, k);

  # Ploting the decision boundary
  #----------------------------------------
  figure = plt.figure(figsize=(15, 9));
  slsl, swsw = np.meshgrid( sl, sw );
  Z = knn.predict( np.c_[ slsl.ravel(), swsw.ravel() ] );
  Z = Z.reshape(slsl.shape);

  ax = plt.subplot(2, 3, 1);
  cf = ax.contourf(slsl, swsw, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~length~(cm)$"], X_train[r"$sepal~width~(cm)$"], c=y_train, cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~length~(cm)$"); ax.set_ylabel(r"$sepal~width~(cm)$");
  ax.set_xlim(slsl.min(), slsl.max());  ax.set_ylim(swsw.min(), swsw.max());
  #ax.set_xticks(()); ax.set_yticks(()); # without ticks
  ax.legend(handles=leg.legend_elements()[0], labels=labels, loc="best", fontsize=10);
  plt.colorbar(cf, ax=ax);
  print("************************************"); print(" \n");

  #  pl  vs. sl
  #========================================
  # Training and testing the knn
  #----------------------------------------
  X_train, X_test, y_train, y_test = train_test_split( data_df.loc[:, (r"$sepal~length~(cm)$", r"$petal~length~(cm)$") ], lab_df, test_size=prc, shuffle=True, random_state=42 );
  print("Training knn: k"+str(k)+", train data "+str((1-prc)*100)+"%, parameters: ");
  print( data_df.loc[:, (r"$sepal~length~(cm)$", r"$petal~length~(cm)$") ] );
  knn, scr_slpl = knn_clf(X_train, y_train, X_test, y_test, k);

  # Ploting the decision boundary
  #----------------------------------------
  slsl, plpl = np.meshgrid( sl, pl );
  Z = knn.predict( np.c_[ slsl.ravel(), plpl.ravel() ] );
  Z = Z.reshape(slsl.shape);

  ax = plt.subplot(2, 3, 2);
  cf = ax.contourf(slsl, plpl, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~length~(cm)$"], X_train[r"$petal~length~(cm)$"], c=y_train, cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~length~(cm)$"); ax.set_ylabel(r"$petal~length~(cm)$");
  ax.set_xlim(slsl.min(), slsl.max());  ax.set_ylim(plpl.min(), plpl.max());
  print("************************************"); print(" \n");

  #  pw  vs. sl
  #========================================
  # Training and testing the knn
  #----------------------------------------
  X_train, X_test, y_train, y_test = train_test_split( data_df.loc[:, (r"$sepal~length~(cm)$", r"$petal~width~(cm)$") ], lab_df, test_size=prc, shuffle=True, random_state=42 );
  print("Training knn: k"+str(k)+", train data "+str((1-prc)*100)+"%, parameters: ");
  print( data_df.loc[:, (r"$sepal~length~(cm)$", r"$petal~width~(cm)$") ] );
  knn, scr_slpw = knn_clf(X_train, y_train, X_test, y_test, k);

  # Ploting the decision boundary
  #----------------------------------------
  slsl, pwpw = np.meshgrid( sl, pw );
  Z = knn.predict( np.c_[ slsl.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(slsl.shape);

  ax = plt.subplot(2, 3, 3);
  cf = ax.contourf(slsl, pwpw, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~length~(cm)$"], X_train[r"$petal~width~(cm)$"], c=y_train, cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~length~(cm)$"); ax.set_ylabel(r"$petal~width~(cm)$");
  ax.set_xlim(slsl.min(), slsl.max());  ax.set_ylim(pwpw.min(), pwpw.max());
  print("************************************"); print(" \n");

  #  pl  vs. sw
  #========================================
  # Training and testing the knn
  #----------------------------------------
  X_train, X_test, y_train, y_test = train_test_split( data_df.loc[:, (r"$sepal~width~(cm)$", r"$petal~length~(cm)$") ], lab_df, test_size=prc, shuffle=True, random_state=42 );
  print("Training knn: k"+str(k)+", train data "+str((1-prc)*100)+"%, parameters: ");
  print( data_df.loc[:, (r"$sepal~width~(cm)$", r"$petal~length~(cm)$") ] );
  knn, scr_swpl = knn_clf(X_train, y_train, X_test, y_test, k);

  # Ploting the decision boundary
  #----------------------------------------
  swsw, plpl = np.meshgrid( sw, pl );
  Z = knn.predict( np.c_[ swsw.ravel(), plpl.ravel() ] );
  Z = Z.reshape(swsw.shape);

  ax = plt.subplot(2, 3, 4);
  cf = ax.contourf(swsw, plpl, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~width~(cm)$"], X_train[r"$petal~length~(cm)$"], c=y_train, cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~width~(cm)$"); ax.set_ylabel(r"$petal~length~(cm)$");
  ax.set_xlim(swsw.min(), swsw.max());  ax.set_ylim(plpl.min(), plpl.max());
  print("************************************"); print(" \n");

  #  pw  vs. sw
  #========================================
  # Training and testing the knn
  #----------------------------------------
  X_train, X_test, y_train, y_test = train_test_split( data_df.loc[:, (r"$sepal~width~(cm)$", r"$petal~width~(cm)$") ], lab_df, test_size=prc, shuffle=True, random_state=42 );
  print("Training knn: k"+str(k)+", train data "+str((1-prc)*100)+"%, parameters: ");
  print( data_df.loc[:, (r"$sepal~width~(cm)$", r"$petal~width~(cm)$") ] );
  knn, scr_swpw = knn_clf(X_train, y_train, X_test, y_test, k);

  # Ploting the decision boundary
  #----------------------------------------
  swsw, pwpw = np.meshgrid( sw, pw );
  Z = knn.predict( np.c_[ swsw.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(swsw.shape);

  ax = plt.subplot(2, 3, 5);
  cf = ax.contourf(swsw, pwpw, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~width~(cm)$"], X_train[r"$petal~width~(cm)$"], c=y_train, cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~width~(cm)$"); ax.set_ylabel(r"$petal~width~(cm)$");
  ax.set_xlim(swsw.min(), swsw.max());  ax.set_ylim(pwpw.min(), pwpw.max());
  print("************************************"); print(" \n");

  #  pw  vs. pl
  #========================================
  # Training and testing the knn
  #----------------------------------------
  X_train, X_test, y_train, y_test = train_test_split( data_df.loc[:, (r"$petal~length~(cm)$", r"$petal~width~(cm)$") ], lab_df, test_size=prc, shuffle=True, random_state=42 );
  print("Training knn: k"+str(k)+", train data "+str((1-prc)*100)+"%, parameters: ");
  print( data_df.loc[:, (r"$petal~length~(cm)$", r"$petal~width~(cm)$") ] );
  knn, scr_plpw = knn_clf(X_train, y_train, X_test, y_test, k);

  # Ploting the decision boundary
  #----------------------------------------
  plpl, pwpw = np.meshgrid( pl, pw );
  Z = knn.predict( np.c_[ plpl.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(plpl.shape);

  ax = plt.subplot(2, 3, 6);
  cf = ax.contourf(plpl, pwpw, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$petal~length~(cm)$"], X_train[r"$petal~width~(cm)$"], c=y_train, cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$petal~length~(cm)$"); ax.set_ylabel(r"$petal~width~(cm)$");
  ax.set_xlim(plpl.min(), plpl.max());  ax.set_ylim(pwpw.min(), pwpw.max());
  print("************************************"); print(" \n");

  # Saving the image
  plt.tight_layout();
  # k: number of  neighbors, td: train data percentage.
  plt.savefig("img_2par/knn_k"+str(k)+"_td"+str( int( (1-prc)*100 ) )+".png", bbox_inches="tight");
  plt.show();

  scr_lt = [scr_slsw, scr_slpl, scr_slpw, scr_swpl, scr_swpw, scr_plpw];
  return scr_lt;


# Plot the KNN decision boundaries training 4 parameters
def knn_dec(data_df, lab_df, prc, k):
  ''' Function for plotting the KNN decision boundaries.  
      data_df: data set in Pandas DataFrame format.
      lab_df: Data label in Pandas DataFrame format.
      prc: Percentage data for testing.
      k: number of neighbors.                         '''

  labels=[r"$setosa$", r"$versicolor$", r"$virginica$"]; # labels for 3 classes

  # Min, max and mean from data frame.
  #----------------------------------------
  (sl_min, sl_max, sl_mean, sw_min, sw_max, sw_mean, pl_min, pl_max, pl_mean, pw_min, pw_max, pw_mean) =  df_min_max_mean(data_df);
  print("                            ");
  # sl, sw, pl, pw  arange for meshgrid
  #----------------------------------------
  sl = np.arange( sl_min-0.1, sl_max+0.1, 0.01);
  sw = np.arange( sw_min-0.1, sw_max+0.1, 0.01);
  pl = np.arange( pl_min-0.1, pl_max+0.1, 0.01);
  pw = np.arange( pw_min-0.1, pw_max+0.1, 0.01);

  # Training and testing the knn
  #----------------------------------------
  X_train, X_test, y_train, y_test = train_test_split(data_df, lab_df, test_size=prc, shuffle=True, random_state=42 );
  print("Training knn: k"+str(k)+", train data "+str((1-prc)*100)+"%, parameters: ");
  knn, scr = knn_clf(X_train, y_train, X_test, y_test, k);
  train_df = X_train.copy(deep=True);  train_df["labels"] = y_train;

  # sw  vs. sl
  #========================================
  # Ploting the decision boundary
  #----------------------------------------
  figure = plt.figure(figsize=(15, 9));

  slsl, swsw = np.meshgrid( sl, sw );
  plpl = np.ones(slsl.shape, dtype=float) * pl_mean;
  pwpw = np.ones(slsl.shape, dtype=float) * pw_mean;

  Z = knn.predict( np.c_[ slsl.ravel(), swsw.ravel(),  plpl.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(slsl.shape);
  is_in =  fun_is(train_df, Z); # Samples that belongs to each plane

  ax = plt.subplot(2, 3, 1);
  cf = ax.contourf(slsl, swsw, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~length~(cm)$"][is_in], X_train[r"$sepal~width~(cm)$"][is_in], c=y_train[is_in], cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~length~(cm)$"); ax.set_ylabel(r"$sepal~width~(cm)$");
  ax.set_xlim(slsl.min(), slsl.max());  ax.set_ylim(swsw.min(), swsw.max());
  ax.legend(handles=leg.legend_elements()[0], labels=labels, loc="best", fontsize=10);
  plt.colorbar(cf, ax=ax);

  # pl  vs. sl
  #========================================
  # Ploting the decision boundary
  #----------------------------------------
  slsl, plpl = np.meshgrid( sl, pl );
  swsw = np.ones(slsl.shape, dtype=float) * sw_mean;
  pwpw = np.ones(slsl.shape, dtype=float) * pw_mean;

  Z = knn.predict( np.c_[ slsl.ravel(), swsw.ravel(),  plpl.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(slsl.shape);
  is_in =  fun_is(train_df, Z); # Samples that belongs to each plane

  ax = plt.subplot(2, 3, 2);
  cf = ax.contourf(slsl, plpl, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~length~(cm)$"][is_in], X_train[r"$petal~length~(cm)$"][is_in], c=y_train[is_in], cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~length~(cm)$"); ax.set_ylabel(r"$petal~length~(cm)$");
  ax.set_xlim(slsl.min(), slsl.max());  ax.set_ylim(plpl.min(), plpl.max());

  # pw  vs. sl
  #========================================
  # Ploting the decision boundary
  #----------------------------------------
  slsl, pwpw = np.meshgrid( sl, pw );
  swsw = np.ones(slsl.shape, dtype=float) * sw_mean;
  plpl = np.ones(slsl.shape, dtype=float) * pl_mean;

  Z = knn.predict( np.c_[ slsl.ravel(), swsw.ravel(),  plpl.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(slsl.shape);
  is_in =  fun_is(train_df, Z); # Samples that belongs to each plane

  ax = plt.subplot(2, 3, 3);
  cf = ax.contourf(slsl, pwpw, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~length~(cm)$"][is_in], X_train[r"$petal~width~(cm)$"][is_in], c=y_train[is_in], cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~length~(cm)$"); ax.set_ylabel(r"$petal~width~(cm)$");
  ax.set_xlim(slsl.min(), slsl.max());  ax.set_ylim(pwpw.min(), pwpw.max());

  # pl vs. sw
  #========================================
  # Ploting the decision boundary
  #----------------------------------------
  swsw, plpl = np.meshgrid( sw, pl  );
  slsl = np.ones(swsw.shape, dtype=float) * sl_mean;
  pwpw = np.ones(swsw.shape, dtype=float) * pw_mean;

  Z = knn.predict( np.c_[ slsl.ravel(), swsw.ravel(),  plpl.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(swsw.shape);
  is_in =  fun_is(train_df, Z); # Samples that belongs to each plane

  ax = plt.subplot(2, 3, 4);
  cf = ax.contourf(swsw, plpl, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~width~(cm)$"][is_in], X_train[r"$petal~length~(cm)$"][is_in], c=y_train[is_in], cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~width~(cm)$"); ax.set_ylabel(r"$petal~length~(cm)$");
  ax.set_xlim(swsw.min(), swsw.max());  ax.set_ylim(plpl.min(), plpl.max());

  # pw vs. sw
  #========================================
  # Ploting the decision boundary
  #----------------------------------------
  swsw, pwpw = np.meshgrid( sw, pw );
  slsl = np.ones(swsw.shape, dtype=float) * sl_mean;
  plpl = np.ones(swsw.shape, dtype=float) * pl_mean;

  Z = knn.predict( np.c_[ slsl.ravel(), swsw.ravel(),  plpl.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(swsw.shape);
  is_in =  fun_is(train_df, Z); # Samples that belongs to each plane

  ax = plt.subplot(2, 3, 5);
  cf = ax.contourf(swsw, pwpw, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$sepal~width~(cm)$"][is_in], X_train[r"$petal~width~(cm)$"][is_in], c=y_train[is_in], cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$sepal~width~(cm)$"); ax.set_ylabel(r"$petal~width~(cm)$");
  ax.set_xlim(swsw.min(), swsw.max());  ax.set_ylim(pwpw.min(), pwpw.max());

  # pw  vs. pl
  #========================================
  # Ploting the decision boundary
  #----------------------------------------
  plpl, pwpw = np.meshgrid( pl, pw );
  slsl = np.ones(plpl.shape, dtype=float) * sl_mean;
  swsw = np.ones(plpl.shape, dtype=float) * sw_mean;

  Z = knn.predict( np.c_[ slsl.ravel(), swsw.ravel(),  plpl.ravel(), pwpw.ravel() ] );
  Z = Z.reshape(plpl.shape);
  is_in =  fun_is(train_df, Z); # Samples that belongs to each plane

  ax = plt.subplot(2, 3, 6);
  cf = ax.contourf(plpl, pwpw, Z, cmap="jet", alpha=0.8); # Boundaries
  leg = ax.scatter(X_train[r"$petal~length~(cm)$"][is_in], X_train[r"$petal~width~(cm)$"][is_in], c=y_train[is_in], cmap="jet", edgecolors='k');
  ax.set_xlabel(r"$petal~length~(cm)$"); ax.set_ylabel(r"$petal~width~(cm)$");
  ax.set_xlim(plpl.min(), plpl.max());  ax.set_ylim(pwpw.min(), pwpw.max());

  # Saving the image
  plt.tight_layout();
  # k: number of  neighbors, td: train data percentage.
  plt.savefig("img/knn_k"+str(k)+"_td"+str( int( (1-prc)*100 ) )+".png", bbox_inches="tight");
  plt.show();

  print("************************************"); print(" \n");

  return scr;


# Score plot
def scr_plot(scr_df, prc):
  ''' This function plots the scores from differents 
      k-values and planes. 
      scr_df: score in Pandas DataFrame format.       '''

  scr_lt = [r"$sw_sl$", r"$pl_sl$", r"$pw_sl$", r"$pl_sw$", r"$pw_sw$", r"$pw_pl$"];
  scr_df = pd.DataFrame(data=scr_df, columns=scr_lt);
  print("Scores data frame: ", scr_df);
  k_lt = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; 

  figure = plt.figure(figsize=(15, 9));

  ax = plt.subplot(2, 3, 1); 
  plt.scatter(k_lt, scr_df[r"$sw_sl$"], cmap="jet", edgecolors='k');
  plt.plot(k_lt, scr_df[r"$sw_sl$"]);
  ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~sw~vs.~sl$");
  ax.set_ylim( scr_df[r"$sw_sl$"].min()-0.2, scr_df[r"$sw_sl$"].max()+0.1 );

  ax = plt.subplot(2, 3, 2); 
  plt.scatter(k_lt, scr_df[r"$pl_sl$"], cmap="jet", edgecolors='k');
  plt.plot(k_lt, scr_df[r"$pl_sl$"]);
  ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~pl~vs.~sl$");
  ax.set_ylim( scr_df[r"$pl_sl$"].min()-0.2, scr_df[r"$pl_sl$"].max()+0.1 );

  ax = plt.subplot(2, 3, 3); 
  plt.scatter(k_lt, scr_df[r"$pw_sl$"], cmap="jet", edgecolors='k');
  plt.plot(k_lt, scr_df[r"$pw_sl$"]);
  ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~pw~vs.~sl$");
  ax.set_ylim( scr_df[r"$pw_sl$"].min()-0.2, scr_df[r"$pw_sl$"].max()+0.1 );

  ax = plt.subplot(2, 3, 4); 
  plt.scatter(k_lt, scr_df[r"$pl_sw$"], cmap="jet", edgecolors='k');
  plt.plot(k_lt, scr_df[r"$pl_sw$"]);
  ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~pl~vs.~sw$");
  ax.set_ylim( scr_df[r"$pl_sw$"].min()-0.2, scr_df[r"$pl_sw$"].max()+0.1 );

  ax = plt.subplot(2, 3, 5); 
  plt.scatter(k_lt, scr_df[r"$pw_sw$"], cmap="jet", edgecolors='k');
  plt.plot(k_lt, scr_df[r"$pw_sw$"]);
  ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~pw~vs.~sw$");
  ax.set_ylim( scr_df[r"$pw_sw$"].min()-0.2, scr_df[r"$pw_sw$"].max()+0.1 );

  ax = plt.subplot(2, 3, 6); 
  plt.scatter(k_lt, scr_df[r"$pw_pl$"], cmap="jet", edgecolors='k');
  plt.plot(k_lt, scr_df[r"$pw_pl$"]);
  ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score$");
  ax.set_xlabel(r"$k~values$"); ax.set_ylabel(r"$score~pw~vs.~pl$");
  ax.set_ylim( scr_df[r"$pw_pl$"].min()-0.2, scr_df[r"$pw_pl$"].max()+0.1 );

  # Saving the image
  plt.tight_layout();

  # k: number of  neighbors, td: train data percentage.
  plt.savefig("img_2par/knn_td"+str( int( (1-prc)*100 ) )+".png", bbox_inches="tight");
  plt.show();

